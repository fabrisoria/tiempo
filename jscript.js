//Variables sobre los datos del tiempo

var lati;
var long;
var tiempo;
var icon;
var gradosC;
var gradosK;
var temp_max;
var maxC;
var temp_min;
var minC;
var ciudad;
var presion;
var humedad;
var visib;
var viento;

// Ubicacion

var onSuccess = function(position) {
            lati=position.coords.latitude;
            long=position.coords.longitude;
            $("#button").show();
        };

        
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError);


function llamar(){
 $.getJSON("http://api.openweathermap.org/data/2.5/weather?lat=" + lati + "&lon=" + long + "&APPID=ac8988e59688d205e50f1358752565ef" , function(clima){
   tiempo = clima;
  //las variables que quieren para saber los datos concretos del tiempo, aqui se recorre el json de openweathermap
   gradosK = tiempo.main.temp;
   icon = tiempo.weather[0].icon;
   gradosC = gradosK - 273.15;
   gradosC = gradosC.toFixed(1);
   ciudad = tiempo.name;
   temp_max = tiempo.main.temp_max;
   maxC = temp_max - 273.15;
   maxC = maxC.toFixed(1);
   temp_min = tiempo.main.temp_min;
   minC = temp_min -273.15;
   minC = minC.toFixed(1);
   presion = tiempo.main.pressure;
   humedad = tiempo.main.humidity;
   visib = tiempo.main.visibility;
   viento = tiempo.main.wind_speed;
   //fin de las variables 
   
   
      $("#grados").html('<h2 class="grados">' + gradosC + " °C </h2> " + "<img class=" +  '"img-responsive" ' + "src=" + "http://openweathermap.org/img/w/"+ icon + ".png>");
         $("#ciudad").html('<h2 class="ciudad">' + ciudad + "</h2> " );
         $("#max_min").html('<p class="max_min">'+ "Max:" + maxC +" ºC " + " / " + "Min:" + minC +" ºC " +"</p> ");
         $("#presion").html('<p class="presion">'+ "Presión: " + presion + " hPa " + "</p> " );
         $("#humedad").html('<p class="humedad">'+ "Humedad: " + humedad + " % " + "</p> " );
         $("#visib").html('<p class="visib">'+ "Visibilidad: " + visib + " m " + "</p> " );
         $("#viento").html('<p class="viento">'+ "Viento: " + viento + " m/s " + "</p> " );
             

   });
}
    

  